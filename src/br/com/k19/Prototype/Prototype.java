package br.com.k19.Prototype;

public interface Prototype<T> {
	T clone();
}
