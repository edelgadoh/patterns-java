package br.com.k19.Command;

import java.util.ArrayList;
import java.util.List;

//Invoker
public class ListaDeComandos {
	private List<Comando> comandos = new ArrayList<>();
	
	public void adiciona(Comando comando){
		this.comandos.add(comando);
	}
	
	public void executa(){
		for(Comando comando : this.comandos){
			comando.executa();
		}
	}
	
}
