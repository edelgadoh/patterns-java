package br.com.k19.Command;

//Concrete command
public class DiminuindoVolumeComando implements Comando {
	private Player player;
	private int level;
	
	public DiminuindoVolumeComando(Player player, int level) {
		this.player = player;
		this.level = level;
	}

	@Override
	public void executa() {
		this.player.decreaseVolume(this.level);

	}

}
