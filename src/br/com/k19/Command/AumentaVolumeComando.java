package br.com.k19.Command;

//Concrete command
public class AumentaVolumeComando implements Comando {
	private Player player;
	private int level;
	
	public AumentaVolumeComando(Player player, int level) {
		this.player = player;
		this.level = level;
	}

	@Override
	public void executa() {
		this.player.increaseVolume(this.level);

	}

}
