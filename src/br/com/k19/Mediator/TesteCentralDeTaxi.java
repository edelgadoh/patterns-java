package br.com.k19.Mediator;

public class TesteCentralDeTaxi {

	public static void main(String[] args) {
		CentralDeTaxi central = new CentralDeTaxi();
		
		Passageiro p1 = new Passageiro("Edwin D", central);
		Passageiro p2 = new Passageiro("Marisol S", central);
		Passageiro p3 = new Passageiro("Jonas Hirata", central);
		
		Taxi t1 = new Taxi(central);
		central.adicionaTaxiDisponivel(t1);
		
		Taxi t2 = new Taxi(central);
		central.adicionaTaxiDisponivel(t2);
		
		
		new Thread(p1).start();
		new Thread(p2).start();
		new Thread(p3).start();
		

	}

}
