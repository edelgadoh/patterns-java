package br.com.k19.Bridge;

import java.io.FileNotFoundException;
import java.io.PrintStream;
//Concrete implementor
public class GeradorDeArquivoTXT implements GeradorDeArquivo {

	@Override
	public void gera(String contudo) {
		try{
			PrintStream saida = new PrintStream("arquivo.txt");
			saida.println(contudo);
			saida.close();
		} catch(FileNotFoundException e){
			e.printStackTrace();
		}
	}

}
