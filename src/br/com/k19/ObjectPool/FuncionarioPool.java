package br.com.k19.ObjectPool;

import java.util.ArrayList;
import java.util.List;

public class FuncionarioPool implements Pool<Funcionario> {
	private List<Funcionario> funcionarios;
	
	public FuncionarioPool(){
		this.funcionarios = new ArrayList<>();
		this.funcionarios.add(new Funcionario("Edwin D"));
		this.funcionarios.add(new Funcionario("Mary S"));
		this.funcionarios.add(new Funcionario("Jonas H"));
		
	}
	
	public Funcionario acquire(){
		if(this.funcionarios.size() > 0){
			return this.funcionarios.remove(0);
		} else {
			return null;
		}
	}
	
	public void release(Funcionario funcionario){
		this.funcionarios.add(funcionario);
	}
	
}
