package br.com.k19.ObjectPool;

public interface Pool<T> {
	T acquire();
	void release(T t);
}
