package br.com.k19.Builder;

import java.util.Calendar;

//Product
public interface Boleto {
	String getSacado();
	String getCedente();
	double getValor();
	Calendar getVencimento();
	int getNossoNumero();
	String toString();
}