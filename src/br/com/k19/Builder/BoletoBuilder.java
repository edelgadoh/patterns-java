package br.com.k19.Builder;

import java.util.Calendar;

//Builder
public interface BoletoBuilder {
	void buildSacado(String sacado);
	void buildCedente(String cedente);
	void buildValor(double valor);
	void buildVencimento(Calendar vencimento);
	void buildNossoNumero(int nossoNumero);
	Boleto 	getBoleto();
}
