package br.com.k19.TemplateMethod;

public class TestaContas {

	public static void main(String[] args) {
		Conta contaCorrente = new ContaCorrente();
		Conta contaPoupanca = new ContaPoupanca();

		contaCorrente.deposita(100);
		contaCorrente.saca(10);
		
		contaPoupanca.deposita(100);
		contaPoupanca.saca(10);
		
		System.out.println("Saldo em conta corrente "+contaCorrente.getSaldo());
		
		System.out.println("Saldo em conta poupanca "+contaPoupanca.getSaldo());
		
	}

}
