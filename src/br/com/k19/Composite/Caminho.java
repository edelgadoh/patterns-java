package br.com.k19.Composite;

import java.util.ArrayList;
import java.util.List;

//Composite
public class Caminho implements Trecho {
	private List<Trecho> trechos;
	
	public Caminho(){
		this.trechos = new ArrayList<>();
	}
	
	public void adiciona(Trecho trecho){
		this.trechos.add(trecho);
	}
	
	public void remove(Trecho trecho){
		this.trechos.remove(trecho);
	}
	
	@Override
	public void imprime() {
		for(Trecho trecho : this.trechos){
			trecho.imprime();
		}

	}

}
