package br.com.k19.AbstractMethod;

//Concrete product
public class ReceptorMastercard implements Receptor {

	@Override
	public String recebe() {
		System.out.println("Recebendo mensagem da mastercard");
		String mensagem = "mensagem da Mastercard";
		return mensagem;
	}

}
