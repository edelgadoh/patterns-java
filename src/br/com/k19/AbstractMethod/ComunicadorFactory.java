package br.com.k19.AbstractMethod;

//AbstractFactory
public interface ComunicadorFactory {
	Emissor createEmissor();
	Receptor createReceptor();
}
