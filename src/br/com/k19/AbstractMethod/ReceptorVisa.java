package br.com.k19.AbstractMethod;

//Concrete product
public class ReceptorVisa implements Receptor{

	@Override
	public String recebe() {
		System.out.println("Recebendo mensagem da visa");
		String mensagem = "mensagem da Visa";
		return mensagem;
	}

}
