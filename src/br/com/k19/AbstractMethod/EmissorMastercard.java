package br.com.k19.AbstractMethod;

//Concrete product
public class EmissorMastercard implements Emissor {

	@Override
	public void envia(String mensagem) {
		System.out.println("Enviando a seguinte mensagem para a mastercard");
		System.out.println(mensagem);

	}

}
