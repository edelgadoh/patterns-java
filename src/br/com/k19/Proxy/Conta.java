package br.com.k19.Proxy;

//Subject
public interface Conta {
	void deposita(double valor);
	void saca(double valor);
	double getSaldo();
}
