package br.com.k19.Decorator;

//Concrete component
public class EmissorBasico implements Emissor {

	@Override
	public void envia(String mensagem) {
		System.out.println("Enviando uma mensagem: ");
		System.out.println(mensagem);

	}

}
