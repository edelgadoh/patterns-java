package br.com.k19.Decorator;
//Decorator
public abstract class EmissorDecorator implements Emissor {
	private Emissor emissor;
	
	
	public EmissorDecorator(Emissor emissor) {
		super();
		this.emissor = emissor;
	}

	public abstract void envia(String mensagem); 
	
	public Emissor getEmissor(){
		return this.emissor;
	}
	
}
