package br.com.k19.Decorator;
//Concrete decorator
public class EmissorDecoratorComCriptografia extends EmissorDecorator {

	public EmissorDecoratorComCriptografia(Emissor emissor) {
		super(emissor);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void envia(String mensagem) {
		System.out.println("Enviando uma mensagem criptografada: ");
		this.getEmissor().envia(criptografa(mensagem));
		
	}
	
	private String criptografa(String mensagem){
		String mensagemCriptografada = new StringBuilder(mensagem).reverse().toString();
		return mensagemCriptografada;
	}

}
