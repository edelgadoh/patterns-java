package br.com.k19.Flyweight;

public class TestaTemas {
	public static void main(String [] args){
		Apresentacao a = new Apresentacao();
		a.adicionaSlide(new Slide(TemaFlyweightFactory.getTema(TemaFlyweightFactory.K19),"K11 - OOP","Com este curso você vai obter uma base sólida de conhecimentos em Java e de OO"));
		a.adicionaSlide(new Slide(TemaFlyweightFactory.getTema(TemaFlyweightFactory.ASTERISCO),"K12 - JSF2 e JPA2","Com este curso você estará apto para desenvolver aplicações web e padrões na plataforma Java"));
		a.adicionaSlide(new Slide(TemaFlyweightFactory.getTema(TemaFlyweightFactory.HIFEN),"K21 - JPA2 e Hibernate","Com este curso você vai obter experiencia profunda em persistência do JPA2 e com o Hibernate, lazzy e full"));
		
		a.imprime();
		
	}
}
