package br.com.k19.Flyweight;
//Flyweight
public interface TemaFlyweight {
	void imprime(String titulo, String texto);
}
