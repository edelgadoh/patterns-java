package br.com.k19.Flyweight;

import java.util.HashMap;
import java.util.Map;
//Flyweight Factory
public class TemaFlyweightFactory {
	private static Map<Class<? extends TemaFlyweight>, TemaFlyweight> temas = new HashMap<>();
	public static final Class<TemaAsterisco> ASTERISCO = TemaAsterisco.class;
	public static final Class<TemaHifen> HIFEN = TemaHifen.class;
	public static final Class<TemaK19> K19 = TemaK19.class;
	
	public static TemaFlyweight getTema(Class<? extends TemaFlyweight> clazz){
		if(!temas.containsKey(clazz)){
			try{
				temas.put(clazz, clazz.newInstance());				
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		return temas.get(clazz);
	}
	
	
}
