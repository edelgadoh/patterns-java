package br.com.k19.Visitor;

public class Telefonista extends Funcionario implements Atualizavel {
	private int ramal;
	
	public Telefonista(String nome, double salario, int ramal) {
		super(nome, salario);
		this.ramal = ramal;
	}
	
	public int getRamal(){
		return ramal;
	}
	
	@Override
	public void aceita(AtualizadorDeFuncionario atualizador) {
		atualizador.atualiza(this);
		
	}

}
