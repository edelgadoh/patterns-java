package br.com.k19.Visitor;

import java.util.ArrayList;
import java.util.List;

public class TestaAtualizadorSalarial {

	public static void main(String[] args) {
		List<Departamento> lista = new ArrayList<>();
		Departamento departamento = new Departamento("Departamento 1");
		Gerente gerente = new Gerente("gerente 1", 4500, "1234");
		Telefonista telefonista = new Telefonista("Telefonista", 1000, 2);
		departamento.getFuncionarios().add(gerente);
		departamento.getFuncionarios().add(telefonista);
		
		lista.add(departamento);
		
		Departamento departamento2 = new Departamento("Departamento 2");
		Gerente gerente2 = new Gerente("gerente 2", 4800, "1234");
		Gerente gerente3 = new Gerente("gerente 3", 4850, "1234");
		Telefonista telefonista2 = new Telefonista("Telefonista 2", 1100, 1);
		departamento2.getFuncionarios().add(gerente2);
		departamento2.getFuncionarios().add(gerente3);
		departamento2.getFuncionarios().add(telefonista2);
		
		lista.add(departamento2);

		AtualizadorDeFuncionario atualizador = new AtualizadorSalarial();
		
		for(Departamento d: lista){
			d.aceita(atualizador);
		}
		
		for(Departamento d: lista){
			for(Funcionario f: d.getFuncionarios()){
				System.out.println("Nome: "+f.getNome()+" - Sálario: "+f.getSalario());
			}
		}
		
	}

}
