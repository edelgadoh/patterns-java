package br.com.k19.Visitor;

public interface AtualizadorDeFuncionario {
	void atualiza(Gerente g);
	void atualiza(Telefonista t);
}
