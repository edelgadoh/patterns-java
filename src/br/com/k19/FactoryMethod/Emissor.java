package br.com.k19.FactoryMethod;

public interface Emissor {
	void envia(String mensagem);
}
