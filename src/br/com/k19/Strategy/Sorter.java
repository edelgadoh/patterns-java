package br.com.k19.Strategy;

import java.util.List;

public interface Sorter {
	<T extends Comparable<? super T>> List<T> sort(List<T> list);
}
